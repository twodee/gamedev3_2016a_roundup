﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
  public float speed;
  public float jumpForce;
  public LayerMask upableLayer;

  private new Rigidbody rigidbody;
  private Animator animator;

  public Transform liftPivot;
  private GameObject crate = null;

  void Start() {
    rigidbody = GetComponent<Rigidbody>(); 
    animator = GetComponent<Animator>(); 
  }
  
  void Update() {
    if (crate == null && Input.GetButtonDown("Jump")) {
      rigidbody.AddForce(jumpForce * Vector3.up);
    }

    if (Input.GetKeyDown(KeyCode.L)) {
      if (crate == null) {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1.5f, 1 << LayerMask.NameToLayer("Pickup"))) {
          crate = hit.collider.gameObject;
          animator.SetBool("IsLifting", true);
        }
      } else {
        animator.SetBool("IsLifting", false);
      }
    }
  }

  public void Attach() {
    // Turn off physics while the crate is being hoisted.
    crate.GetComponent<Rigidbody>().isKinematic = true;

    // We want the crate to be a child of our pivot point so
    // that we can rotate it above the avatar.
    crate.transform.parent = liftPivot;
  }

  public void Detach() {
    // Turn physics back on now that crate has been dropped.
    crate.GetComponent<Rigidbody>().isKinematic = false;

    // The crate is no longer ours. It has graduated.
    crate.transform.parent = null;
    crate = null;
  }

  void FixedUpdate() {
    float turnOomph = Input.GetAxis("Horizontal");
    float forwardOomph = Input.GetAxis("Vertical");

    Vector3 velocity = forwardOomph * transform.forward * speed;
    animator.SetFloat("Speed", velocity.magnitude);

    // Preserve any lingering y-velocity from jumps.
    velocity.y = rigidbody.velocity.y;
    rigidbody.velocity = velocity;
    rigidbody.MoveRotation(rigidbody.rotation * Quaternion.AngleAxis(turnOomph, Vector3.up));

    animator.SetFloat("Upness", rigidbody.velocity.y);
  }
}
